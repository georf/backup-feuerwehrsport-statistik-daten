Feuerwehrsport-Statistik-Daten
==============================
Dies sind die MySQL-Daten der Feuerwehrsport-Statistiken. Sie stehen allen Leuten zur Verfügung, um selber Berechnungen damit anzustellen. Sie werden bei Änderungen aktualisiert.

Die Daten stammen von der Webseite:
http://www.feuerwehrsport-statistik.de
